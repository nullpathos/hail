package eu.nullpathos.hail;

import java.util.Date;

public class Globals {
	private static double latitude = 0, longitude = 0;
	private static Date dateStamp = new Date(0);
	public static final String APP_NAME = "Hail";
	private static String password;
	private static int refreshTime=0, refreshDistance=0;
	private static boolean serviceRunning=false;
	private static boolean locationProviderOn=true;

	public static boolean isLocationProviderOn() {
		return locationProviderOn;
	}

	public static void setLocationProviderOn(boolean locationProviderOn) {
		Globals.locationProviderOn = locationProviderOn;
	}

	public static boolean isServiceRunning() {
		return serviceRunning;
	}

	public static void setServiceRunning(boolean serviceRunning) {
		Globals.serviceRunning = serviceRunning;
	}

	public static int getRefreshTime() {
		return refreshTime;
	}

	public static void setRefreshTime(int refreshTime) {
		Globals.refreshTime = refreshTime;
	}

	public static int getRefreshDistance() {
		return refreshDistance;
	}

	public static void setRefreshDistance(int refreshDistance) {
		Globals.refreshDistance = refreshDistance;
	}

	public static Date getDateStamp() {
		return dateStamp;
	}
	
	public static void setDateStamp(Date dateStamp) {
		Globals.dateStamp = dateStamp;
	}
	
	public static double getLatitude() {
		return latitude;
	}

	public static void setLatitude(double latitude) {
		Globals.latitude = latitude;
	}

	public static double getLongitude() {
		return longitude;
	}

	public static void setLongitude(double longitude) {
		Globals.longitude = longitude;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		Globals.password = password;
	}

}
