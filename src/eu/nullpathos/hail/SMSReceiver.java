/*
    This file is part of Hail from nullpathos.eu.

    Hail is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Hail.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.nullpathos.hail;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {
	private final String TAG = getClass().getSimpleName().toString();
	private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	private Intent mIntent;
	private static int MAX_SMS_MESSAGE_LENGTH = 160;

	// Retrieve SMS
	public void onReceive(Context context, Intent intent) {
		mIntent = intent;
		String smsNumber = "", smsText = "";

		Log.d(TAG, "onReceive");

		String action = intent.getAction();

		if (action.equals(ACTION_SMS_RECEIVED)) {

			SmsMessage[] msgs = getMessagesFromIntent(mIntent);
			if (msgs != null) {
				for (int i = 0; i < msgs.length; i++) {
					smsNumber = msgs[i].getOriginatingAddress();
					smsText += msgs[i].getMessageBody().toString();
					smsText += "\n";
				}
			}

			Log.d(TAG, "message receieved from " + smsNumber + ": " + smsText);

			if (smsText.startsWith(Globals.APP_NAME) && smsText.contains(Globals.getPassword())) {
				Log.d(TAG, "correct password");
				DateFormat df = DateFormat.getDateTimeInstance();
				String dateString = df.format(Globals.getDateStamp());
				String satStatus;
				if (Globals.isLocationProviderOn()) {
					satStatus = "saton";
				} else {
					satStatus = "satoff";
				}
				smsText = String.format(Locale.US, "https://www.openstreetmap.org/#map=15/%f/%f %s %s",
						Globals.getLatitude(), Globals.getLongitude(), dateString, satStatus);
				// example https://www.openstreetmap.org/#map=15/51.4772/0.0000
				// date=.... (15 is the openstreetmap zoom)
				sendSMS(smsNumber, smsText);
			}
		}

	}

	// TODO: understand this function
	public static SmsMessage[] getMessagesFromIntent(Intent intent) {
		Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
		byte[][] pduObjs = new byte[messages.length][];

		for (int i = 0; i < messages.length; i++) {
			pduObjs[i] = (byte[]) messages[i];
		}
		byte[][] pdus = new byte[pduObjs.length][];
		int pduCount = pdus.length;
		SmsMessage[] msgs = new SmsMessage[pduCount];
		for (int i = 0; i < pduCount; i++) {
			pdus[i] = pduObjs[i];
			msgs[i] = SmsMessage.createFromPdu(pdus[i]);
		}
		return msgs;
	}

	// ---sends an SMS message to another device---
	public static void sendSMS(String phoneNumber, String message) {

		SmsManager smsManager = SmsManager.getDefault();

		int length = message.length();
		if (length > MAX_SMS_MESSAGE_LENGTH) {
			ArrayList<String> messagelist = smsManager.divideMessage(message);
			smsManager.sendMultipartTextMessage(phoneNumber, null, messagelist, null, null);
		} else {
			smsManager.sendTextMessage(phoneNumber, null, message, null, null);
		}
	}
}
