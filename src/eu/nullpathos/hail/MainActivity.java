/*
    This file is part of Hail from nullpathos.eu.

    Hail is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Hail.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.nullpathos.hail;

import java.text.DateFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

//public class MainActivity extends ActionBarActivity {
public class MainActivity extends Activity {
	private final String TAG = getClass().getSimpleName().toString();
	TextView textLatitude, textLongitude, textDate, textService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textLatitude = (TextView) findViewById(R.id.textLatitude);
		textLongitude = (TextView) findViewById(R.id.textLongitude);
		textDate = (TextView) findViewById(R.id.textDate);
		textService = (TextView) findViewById(R.id.textService);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG,"onResume");
		textLatitude.setText(String.valueOf(Globals.getLatitude()));
		textLongitude.setText(String.valueOf(Globals.getLongitude()));
		DateFormat df = DateFormat.getDateTimeInstance();
		String dateString = df.format(Globals.getDateStamp());
		textDate.setText(dateString);
		if (Globals.isServiceRunning()){
			textService.setText("service running");
		} else {
			textService.setText("service stopped");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		Log.d(TAG, "onCreateOptionsMenu");
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		Log.d(TAG, "onOptionsItemSelected");
		
		Intent serviceIntent = new Intent(this, HailService.class);
		
		switch (item.getItemId()) {
		case R.id.item_start_service:
			startService(serviceIntent);
			return true;
		case R.id.item_stop_service:
			stopService(serviceIntent);
			return true;
		case R.id.item_settings:
			startActivity(new Intent(this, PrefsActivity.class));
		default:
			return false;
		}
	}
}
