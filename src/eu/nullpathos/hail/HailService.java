/*
    This file is part of Hail from nullpathos.eu.

    Hail is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Hail.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.nullpathos.hail;

import java.util.Date;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class HailService extends Service implements LocationListener {
	private final String TAG = getClass().getSimpleName().toString();
	LocationManager locationManager;
	SharedPreferences prefs;
	private final String LAST_LAT = "last_known_latitude";
	private final String LAST_LONG = "last_known_longitude";
	private final String LAST_DATE = "last_known_date";

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private void saveLastLocation() {
		Log.d(TAG, "storing last known location");
		SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat(LAST_LAT, (float) Globals.getLatitude());
		editor.putFloat(LAST_LONG, (float) Globals.getLongitude());
		editor.putLong(LAST_DATE, Globals.getDateStamp().getTime());
		editor.commit();
		Log.d(TAG, "latitude = " + Globals.getLatitude() + " longitude = " + Globals.getLongitude());
	}

	private void getLastLocation() {
		Log.d(TAG, "getting last known location");
		Globals.setLatitude(prefs.getFloat(LAST_LAT, 0));
		Globals.setLongitude(prefs.getFloat(LAST_LONG, 0));
		Globals.setDateStamp(new Date(prefs.getLong(LAST_DATE, 0)));
		Log.d(TAG, "latitude = " + Globals.getLatitude() + " longitude = " + Globals.getLongitude());
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// get initial location
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
//		@SuppressWarnings("static-access")
//		Location lastLocation = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
//		if (lastLocation != null) {
//			onLocationChanged(lastLocation);
//		} else {
//			// get last loc from prefs
//			getLastLocation();
//		}
		getLastLocation();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Globals.getRefreshTime() * 1000,
				Globals.getRefreshDistance(), this);
		Globals.setServiceRunning(true);
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		locationManager.removeUpdates(this);
		Globals.setServiceRunning(false);
		// store last known location
		saveLastLocation();
		Log.d(TAG, "onDestroy");
	}

	@Override
	public void onLocationChanged(Location location) {
		Globals.setLatitude(location.getLatitude());
		Globals.setLongitude(location.getLongitude());
		Globals.setDateStamp(new Date());
		Log.d(TAG, "onLocationChanged");
		Log.d(TAG, "latitude = " + Globals.getLatitude() + " longitude = " + Globals.getLongitude());
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onProviderEnabled(String provider) {
		Globals.setLocationProviderOn(true);
		// Location lastLocation =
		// locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
		// if (lastLocation != null) {
		// onLocationChanged(lastLocation);
		// }

		// get last loc from prefs
		getLastLocation();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Globals.setLocationProviderOn(false);
		saveLastLocation();
	}

}
