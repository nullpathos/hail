package eu.nullpathos.hail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
	private final String TAG = getClass().getSimpleName().toString();
	SharedPreferences prefs;
	static final boolean DEFAULT_START_ON_BOOT = false;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive");

		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		boolean startOnBoot = prefs.getBoolean("start_on_boot", DEFAULT_START_ON_BOOT);

		if (startOnBoot) {
			context.startService(new Intent(context, HailService.class));
		}
	}
}
