package eu.nullpathos.hail;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.util.Log;

public class HailApp extends Application implements OnSharedPreferenceChangeListener {
	private final String TAG = getClass().getSimpleName().toString();
	SharedPreferences prefs;
	static final String DEFAULT_PASSWORD = "Hail";
	String password = DEFAULT_PASSWORD;
	static final int DEFAULT_REFRESH_TIME = 600, DEFAULT_REFRESH_DISTANCE = 100; // seconds and mitres

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(this);
		Globals.setPassword(prefs.getString("password", DEFAULT_PASSWORD));
		Globals.setRefreshTime(prefs.getInt("refresh_time", DEFAULT_REFRESH_TIME));
		Globals.setRefreshDistance(prefs.getInt("refresh_distance", DEFAULT_REFRESH_DISTANCE));
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Log.d(TAG, "onSharedPreferenceChanged key=" + key);
		if (key.equals("password")) {
			Globals.setPassword(prefs.getString("password", DEFAULT_PASSWORD));
		}
		if (key.equals("refresh_time")) {
			Globals.setRefreshTime(prefs.getInt("refresh_time", DEFAULT_REFRESH_TIME));
			if (Globals.isServiceRunning()) {
				Intent serviceIntent = new Intent(this, HailService.class);
				stopService(serviceIntent);
				startService(serviceIntent);
			}

		}
		if (key.equals("refresh_distance")) {
			Globals.setRefreshDistance(prefs.getInt("refresh_distance", DEFAULT_REFRESH_DISTANCE));
			if (Globals.isServiceRunning()) {
				Intent serviceIntent = new Intent(this, HailService.class);
				stopService(serviceIntent);
				startService(serviceIntent);
			}
		}
	}

}
