# Hail

Hail is an experimental application, built to explore features of the Android framework.

It will reply to an SMS message with the device's GPS coordinates.
The message text must start with "Hail" and contain a password.

Author: nullpathos

License: GPL
